# System Buffers

## What

	-System Buffers version 4.5.0
	-Provides resource pooling of any type for performance-critical applications that allocate and deallocate objects frequently.
	-Commonly Used Types:
	-System.Buffers.ArrayPool<T>

## Requirements
[![Unity 2018.3+](https://img.shields.io/badge/unity-2018.3+-brightgreen.svg?style=flat&logo=unity&cacheSeconds=2592000)](https://unity3d.com/get-unity/download/archive)
[![.NET 2.0 Scripting Runtime](https://img.shields.io/badge/.NET-2.0-blueviolet.svg?style=flat&cacheSeconds=2592000)](https://docs.unity3d.com/2018.3/Documentation/Manual/ScriptingRuntimeUpgrade.html)


## Installation

```bash
"com.yenmoc.system-buffers":"https://gitlab.com/yenmoc/system-buffers"
or
npm publish --registry http://localhost:4873
```

